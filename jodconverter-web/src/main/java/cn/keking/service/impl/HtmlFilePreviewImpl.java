package cn.keking.service.impl;

import cn.keking.service.FilePreview;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

/**
 * @author qiutiandong
 */
@Service
public class HtmlFilePreviewImpl implements FilePreview {

    @Override
    public String filePreviewHandle(String url, Model model) {
        model.addAttribute("ordinaryUrl", url);
        return "txt";
    }
}
