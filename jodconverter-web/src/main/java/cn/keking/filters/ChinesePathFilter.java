package cn.keking.filters;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 *
 * @author yudian-it
 * @date 2017/11/30
 */
public class ChinesePathFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        String pathBuilder = request.getScheme() + "://" + request.getServerName() + ":" +
                request.getServerPort() + ((HttpServletRequest) request).getContextPath() + "/";
        request.setAttribute("baseUrl", pathBuilder);
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
