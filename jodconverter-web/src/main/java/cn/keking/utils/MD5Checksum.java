package cn.keking.utils;

import org.apache.commons.codec.digest.DigestUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * @author qiutiandong
 */
public class MD5Checksum {

    public static String getMD5CheckSum(String filePath) {
        String md5 = null;
        try {
            FileInputStream fileInputStream = new FileInputStream(new File(filePath));
            md5 = DigestUtils.md5Hex(fileInputStream);
            fileInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return md5;
    }

}
